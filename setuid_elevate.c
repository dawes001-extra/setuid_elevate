#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main(int argc, char *argv[],char *envp[]) {
    char **new_argv = malloc((argc-1) * sizeof(char*));
    for (int i = 0; i < argc - 1; i++) new_argv[i] = argv[i+1];
    if (setuid(0))
    {
        perror("setuid");
        return 1;
    }
    execve(new_argv[0], &new_argv[0], envp);
}
